**ClassMate, egy mobil app, ahol meg tudod nézni a leckéket, amiket az osztálytársaid küldtek be**<br>

<img src=https://build.phonegap.com/apps/3418578/badge/1336028252/android.svg />

 **Hasznos linkek**<br>
[Legstabilabb kliens (android)](https://build.phonegap.com/apps/3418578/download/android)<br>
[Elérhető buildek](https://build.phonegap.com/apps/3418578/builds)<br>

<hr>


**Hogyan futtatsd**<br>

*Kliens*<br>
Én buildelem az mobil appot, így azzal nincs sok teendő.<br>

*Szerver*<br>

Függőségek telepítése:
```# pip3 install -r requirements.txt```

Indítás:
```$ python3 manage.py runserver```

Első indítás elött:
```$ python3 manage.py migrate```


*autoTestClient.py*<br>
Arra való, hogy a szerver teljesítményét/működését teszteljük.<br>
Használat: <br>
```$ python3 autoTestClient.py <szerverIP>:<opcionális, szerver port> <opcionális, egy kép útvonala> ```<br>
