// IP_CHECK.JS - felhasználónév ellenőrzése, felhasználónév hiányában gyorsan kérünk egyet

Ip = window.localStorage.getItem("serverIp");
Port = window.localStorage.getItem("serverPort");

if (Ip == null) {
    window.localStorage.setItem("serverIp", "46.139.116.9");
}

if (Port == null) {
    window.localStorage.setItem("serverPort", 8000);
}