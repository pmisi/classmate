from django.db import models
import datetime
import calendar
import locale

# Create your models here.


class Bejegyzes(models.Model):
    uname = models.CharField(verbose_name='felhasználónév', max_length=120)
    het = models.IntegerField(verbose_name='hét száma az évben', default=datetime.date.today().isocalendar()[1])
    nap = models.IntegerField(verbose_name='nap száma a hétben', default=datetime.datetime.today().weekday())
    tant = models.CharField(verbose_name='tantárgy', max_length=120)
    anyag = models.CharField(verbose_name='anyag', max_length=120)
    pic = models.TextField(verbose_name='kép (base64)', max_length=10000, blank=True, null=True)

    # mi jelenjen meg pl. az admin felületen
    def __str__(self):
        # állítsa be, hogy a napok a rendszer nyelvén jelenjenek meg (jóesetben magyar)
        locale.setlocale(locale.LC_ALL, '')
        bejegyzes_name = ''

        # derítse ki, melyik héten lett beírva a bejegyzés, preferálja az írott formát a szám helyett
        if datetime.date.today().isocalendar()[1] == self.het:
            bejegyzes_name += 'Ez a hét '

        elif abs(datetime.date.today().isocalendar()[1] - self.het) == 1 and datetime.date.today().isocalendar()[1] > self.het:
            bejegyzes_name += 'Múlt hét '

        elif abs(datetime.date.today().isocalendar()[1] - self.het) == 1 and datetime.date.today().isocalendar()[1] < self.het:
            bejegyzes_name += 'Következő hét '

        else:
            bejegyzes_name += str(self.het) + '. hét '

        # adja hozzá a meglévő stringhez a nap nevét 'i' vel a végén
        bejegyzes_name += str(list(calendar.day_name)[self.nap]) + 'i '
        # adja hozzá a lánchoz a tantárgy nevét kisbetűvel
        bejegyzes_name += str(self.tant).lower()

        return bejegyzes_name

    class Meta:
        verbose_name = 'bejegyzés'
        verbose_name_plural = 'bejegyzések'


class Motd(models.Model):
    text = models.TextField(verbose_name="szöveg", max_length=50, null=True)
    date = models.DateField(verbose_name="dátum", default=datetime.date.today)

    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'napi üzenet'
        verbose_name_plural = 'napi üzenetek'
