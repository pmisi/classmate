from rest_framework import serializers
from .models import Bejegyzes, Motd


class BejegyzesSerializer(serializers.ModelSerializer):


    class Meta:
        model = Bejegyzes
        fields = '__all__'


class MotdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Motd
        fields = '__all__'
