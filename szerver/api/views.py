from rest_framework.generics import ListAPIView, ListCreateAPIView, DestroyAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Bejegyzes, Motd
from .serializers import BejegyzesSerializer, MotdSerializer
import datetime

# Create your views here.


class BejegyzesekList(ListCreateAPIView):
    queryset = Bejegyzes.objects.all()
    serializer_class = BejegyzesSerializer


class EHetiBejegyzesek(ListAPIView):
    serializer_class = BejegyzesSerializer

    def get_queryset(self):
        return Bejegyzes.objects.filter(het=self.kwargs['het'])


class BejegyzesekTorlese(DestroyAPIView):
    serializer_class = BejegyzesSerializer

    def get_queryset(self):
        return Bejegyzes.objects.filter(id=self.kwargs['pk'])


class MotdList(APIView):
    def get(self, request):
        motd = Motd.objects.filter(date=datetime.date.today()).order_by('-id')  # a frissebb üzenetet mutassuk
        try:
            serializer = MotdSerializer(motd[0])
        except IndexError:
            return Response('')

        return Response(serializer.data['text'])
